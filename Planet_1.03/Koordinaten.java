/**
 * Write a description of class Koordinaten here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Koordinaten  
{
    // instance variables - replace the example below with your own
    private int x1;
    private int x2;
    private int y1;
    private int y2;
    

    /**
     * Constructor for objects of class Koordinaten
     */
    public Koordinaten(int X1, int Y1, int X2, int Y2)
    {
        x1 = X1;
        x2 = X2;
        y2 = Y2;
        y1 = Y1;
    }

    public int getY1()
    {
    return y1;    
    }
    public int getX2()
    {
    return x2;    
    }
    public int getY2()
    {
    return y2;    
    }
    public int getX1()
    {
    return x1;    
    }
}
