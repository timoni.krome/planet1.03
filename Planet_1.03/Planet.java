import greenfoot.*; // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Die einzigen aktiven Akteure in der Roboterwelt sind die Roboter.
 * Die Welt besteht aus 14 * 10 Feldern.
 */

public class Planet extends World
{
    private static int zellenGroesse = 50;

    /**
     * Erschaffe eine Welt mit 15 * 12 Zellen.
     */
    public Planet()
    {
        super(16, 12, zellenGroesse);
        setBackground("images/boden.png");
        setPaintOrder(String.class, Rover.class, Marke.class, Gestein.class, Huegel.class);
        Greenfoot.setSpeed(20); 
        prepare();
    }

    
    /**
     * Prepare the world for the start of the program.
     * That is: create the initial objects and add them to the world.
     */
    private void prepare()
    {
        Huegel huegel = new Huegel();
        addObject(huegel,7,9);
        huegel.setLocation(7,9);
        removeObject(huegel);
        Huegel huege0 = new Huegel();
        addObject(huegel,6,6);
        removeObject(huegel);
        Huegel huege2 = new Huegel();
        addObject(huegel,12,6);
        huegel.setLocation(6,7);
        removeObject(huegel);
        Huegel huege3 = new Huegel();
        addObject(huegel,8,8);
        Huegel huegel2 = new Huegel();
        addObject(huegel2,5,9);
        huegel.setLocation(6,9);
        Huegel huegel3 = new Huegel();
        addObject(huegel3,5,8);
        Huegel huegel4 = new Huegel();
        addObject(huegel4,5,7);
        Huegel huegel5 = new Huegel();
        addObject(huegel5,6,7);
        Huegel huegel6 = new Huegel();
        addObject(huegel6,7,7);
        Huegel huegel7 = new Huegel();
        addObject(huegel7,8,7);
        Huegel huegel8 = new Huegel();
        addObject(huegel8,9,7);
        Huegel huegel9 = new Huegel();
        addObject(huegel9,10,7);
        Huegel huegel10 = new Huegel();
        addObject(huegel10,11,7);
        Huegel huegel11 = new Huegel();
        addObject(huegel11,12,7);
        Huegel huegel12 = new Huegel();
        addObject(huegel12,7,6);
        Huegel huegel13 = new Huegel();
        addObject(huegel13,7,5);
        Huegel huegel14 = new Huegel();
        addObject(huegel14,7,4);
        Huegel huegel15 = new Huegel();
        addObject(huegel15,7,3);
        Huegel huegel16 = new Huegel();
        addObject(huegel16,7,9);
        Huegel huegel17 = new Huegel();
        addObject(huegel17,8,9);
        Huegel huegel18 = new Huegel();
        addObject(huegel18,9,9);
        Huegel huegel19 = new Huegel();
        addObject(huegel19,10,9);
        Huegel huegel20 = new Huegel();
        addObject(huegel20,11,9);
        Huegel huegel21 = new Huegel();
        addObject(huegel21,12,9);
        Huegel huegel22 = new Huegel();
        addObject(huegel22,13,9);
        Huegel huegel23 = new Huegel();
        addObject(huegel23,14,8);
        Huegel huegel24 = new Huegel();
        addObject(huegel24,14,7);
        huegel22.setLocation(13,10);
        huegel21.setLocation(12,10);
        huegel20.setLocation(11,10);
        huegel19.setLocation(10,10);
        huegel18.setLocation(9,10);
        huegel17.setLocation(8,10);
        huegel16.setLocation(7,10);
        huegel.setLocation(6,10);
        huegel2.setLocation(5,10);
        huegel3.setLocation(5,9);
        huegel4.setLocation(5,8);
        huegel5.setLocation(6,8);
        huegel6.setLocation(7,8);
        huegel7.setLocation(8,8);
        huegel8.setLocation(9,8);
        huegel9.setLocation(10,8);
        huegel11.setLocation(11,8);
        huegel10.setLocation(11,8);
        huegel23.setLocation(14,9);
        huegel24.setLocation(14,8);
        huegel12.setLocation(7,7);
        huegel13.setLocation(7,6);
        huegel14.setLocation(7,5);
        huegel15.setLocation(7,4);
        huegel2.setLocation(5,11);
        huegel.setLocation(6,11);
        huegel16.setLocation(7,11);
        huegel17.setLocation(8,11);
        huegel18.setLocation(9,11);
        huegel19.setLocation(10,11);
        huegel20.setLocation(11,11);
        huegel21.setLocation(12,11);
        huegel22.setLocation(13,11);
        huegel2.setLocation(3,11);
        huegel.setLocation(4,11);
        huegel16.setLocation(5,11);
        huegel17.setLocation(6,11);
        huegel18.setLocation(7,11);
        huegel19.setLocation(8,11);
        huegel20.setLocation(9,11);
        huegel21.setLocation(10,11);
        huegel22.setLocation(11,11);
        huegel3.setLocation(3,10);
        huegel4.setLocation(3,9);
        huegel5.setLocation(3,9);
        huegel4.setLocation(4,9);
        huegel6.setLocation(5,9);
        huegel12.setLocation(5,8);
        huegel13.setLocation(5,7);
        huegel14.setLocation(5,6);
        huegel15.setLocation(5,5);
        huegel7.setLocation(6,9);
        huegel8.setLocation(7,9);
        huegel9.setLocation(8,9);
        huegel10.setLocation(9,9);
        huegel11.setLocation(10,9);
        huegel23.setLocation(12,10);
        huegel24.setLocation(12,9);
        Huegel huegel25 = new Huegel();
        addObject(huegel25,12,11);
        Huegel huegel26 = new Huegel();
        addObject(huegel26,12,8);
        Huegel huegel27 = new Huegel();
        addObject(huegel27,12,7);
        Huegel huegel28 = new Huegel();
        addObject(huegel28,11,7);
        Huegel huegel29 = new Huegel();
        addObject(huegel29,10,7);
        Huegel huegel30 = new Huegel();
        addObject(huegel30,9,7);
        Huegel huegel31 = new Huegel();
        addObject(huegel31,8,7);
        Huegel huegel32 = new Huegel();
        addObject(huegel32,7,7);
        Huegel huegel33 = new Huegel();
        addObject(huegel33,6,5);
        Huegel huegel34 = new Huegel();
        addObject(huegel34,7,5);
        Huegel huegel35 = new Huegel();
        addObject(huegel35,8,5);
        Huegel huegel36 = new Huegel();
        addObject(huegel36,9,5);
        Huegel huegel37 = new Huegel();
        addObject(huegel37,10,5);
        Huegel huegel38 = new Huegel();
        addObject(huegel38,11,5);
        Huegel huegel39 = new Huegel();
        addObject(huegel39,12,5);
        Huegel huegel40 = new Huegel();
        addObject(huegel40,13,7);
        Huegel huegel41 = new Huegel();
        addObject(huegel41,14,7);
        Huegel huegel42 = new Huegel();
        addObject(huegel42,15,7);
        Huegel huegel43 = new Huegel();
        addObject(huegel43,15,6);
        Huegel huegel44 = new Huegel();
        addObject(huegel44,15,5);
        Huegel huegel45 = new Huegel();
        addObject(huegel45,15,4);
        Huegel huegel46 = new Huegel();
        addObject(huegel46,15,3);
        Huegel huegel47 = new Huegel();
        addObject(huegel47,13,5);
        Huegel huegel48 = new Huegel();
        addObject(huegel48,14,3);
        Huegel huegel49 = new Huegel();
        addObject(huegel49,13,3);
        Huegel huegel50 = new Huegel();
        addObject(huegel50,12,3);
        Huegel huegel51 = new Huegel();
        addObject(huegel51,11,3);
        Huegel huegel52 = new Huegel();
        addObject(huegel52,10,3);
        Huegel huegel53 = new Huegel();
        addObject(huegel53,8,3);
        Huegel huegel54 = new Huegel();
        addObject(huegel54,8,4);
        Marke marke = new Marke();
        addObject(marke,9,2);
        removeObject(huegel52);
        removeObject(huegel51);
        removeObject(huegel50);
        removeObject(huegel49);
        huegel48.setLocation(14,3);
        removeObject(huegel48);
        removeObject(huegel46);
        removeObject(huegel45);
        removeObject(huegel44);
        removeObject(huegel43);
        removeObject(huegel42);
        removeObject(huegel41);
        removeObject(huegel40);
        removeObject(huegel27);
        removeObject(huegel28);
        removeObject(huegel29);
        removeObject(huegel30);
        removeObject(huegel31);
        removeObject(huegel32);
        removeObject(huegel26);
        removeObject(huegel24);
        removeObject(huegel23);
        removeObject(huegel25);
        removeObject(huegel22);
        removeObject(huegel21);
        removeObject(huegel20);
        removeObject(huegel19);
        removeObject(huegel18);
        removeObject(huegel17);
        removeObject(huegel16);
        removeObject(huegel);
        removeObject(huegel2);
        removeObject(huegel3);
        removeObject(huegel5);
        removeObject(huegel4);
        removeObject(huegel6);
        removeObject(huegel11);
        removeObject(huegel10);
        removeObject(huegel9);
        removeObject(huegel8);
        removeObject(huegel7);
        removeObject(huegel12);
        removeObject(huegel13);
        removeObject(huegel14);
        removeObject(huegel15);
        removeObject(huegel33);
        removeObject(huegel34);
        removeObject(huegel35);
        removeObject(huegel54);
        removeObject(huegel53);
        removeObject(marke);
        removeObject(huegel36);
        removeObject(huegel37);
        removeObject(huegel38);
        removeObject(huegel39);
        removeObject(huegel47);
        Rover rover2 = new Rover("Misuri");
        addObject(rover2,0,1);
        Gestein gestein = new Gestein();
        addObject(gestein,3,4);
        Gestein gestein2 = new Gestein();
        addObject(gestein2,9,2);
        Gestein gestein3 = new Gestein();
        addObject(gestein3,4,7);
        Gestein gestein4 = new Gestein();
        addObject(gestein4,10,5);
        Gestein gestein5 = new Gestein();
        addObject(gestein5,5,8);
        Gestein gestein6 = new Gestein();
        addObject(gestein6,11,4);
        Gestein gestein7 = new Gestein();
        addObject(gestein7,10,4);
        Gestein gestein8 = new Gestein();
        addObject(gestein8,7,4);
        Gestein gestein9 = new Gestein();
        addObject(gestein9,4,4);
        Gestein gestein10 = new Gestein();
        addObject(gestein10,4,6);
        Gestein gestein11 = new Gestein();
        addObject(gestein11,8,6);
        Gestein gestein12 = new Gestein();
        addObject(gestein12,8,6);
        Gestein gestein13 = new Gestein();
        addObject(gestein13,8,8);
        Gestein gestein14 = new Gestein();
        addObject(gestein14,5,9);
        Gestein gestein15 = new Gestein();
        addObject(gestein15,3,8);
        Gestein gestein16 = new Gestein();
        addObject(gestein16,3,4);
        Gestein gestein17 = new Gestein();
        addObject(gestein17,3,6);
        Gestein gestein18 = new Gestein();
        addObject(gestein18,7,2);
        Gestein gestein19 = new Gestein();
        addObject(gestein19,3,2);
        Gestein gestein20 = new Gestein();
        addObject(gestein20,13,3);
        Gestein gestein21 = new Gestein();
        addObject(gestein21,11,9);
        Gestein gestein22 = new Gestein();
        addObject(gestein22,13,8);
        Gestein gestein23 = new Gestein();
        addObject(gestein23,11,10);
        Gestein gestein24 = new Gestein();
        addObject(gestein24,14,8);
        Gestein gestein25 = new Gestein();
        addObject(gestein25,14,6);
        Gestein gestein26 = new Gestein();
        addObject(gestein26,13,4);
        Gestein gestein27 = new Gestein();
        addObject(gestein27,14,1);
    }
}