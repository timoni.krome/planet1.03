import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

public class Rover extends Actor
{
    private Display anzeige;
    private String name;
    private int strecke = 0;
    private int anzahlGefundenerSteine = 0;
    private boolean wasserGefunden= false;
    
    public Rover(String namekonstruktor){
        super();
        name = namekonstruktor;
    }
    public Rover() {
        super();
    }
    public void findeGestein(){
        if(gesteinVorhanden()) {
                    gesteinVorhanden();
                    anzahlGefundenerSteine++;
                    //nachricht("Anzahl Gefundener Steine: "+anzahlGefundenerSteine);
                    if(anzahlGefundenerSteine==1){
                        nachricht("Wasser gefunden");
                    }
                }
    }
    public void fahreAllesab(){
        if(getY()<=11){
            while(getX()<15){
                fahre();
                findeGestein();
            }
            if(getY()!=11){
            drehe("rechts");
            fahre();
            findeGestein();
            drehe("rechts");
            while(getX()>0){
                fahre();
                findeGestein();
            }
            drehe("links");
            fahre();
            findeGestein();
            drehe("links");
            fahreAllesab();
        }
            
         //else{
            //anzeige.anzeigen("Ende des Programmes");
            //System.out.println("Ende des Programmes");
        //}
    }
    }
    
    public void umbennenen(String pName) {
        name = pName;
    }
    
    public void wieHeißtDu(){
        nachricht(name);
    }

    public void act() 
    {

    } 
    public void findOutsideLabyrint() {
    boolean weitermachen = true;
    if(markeVorhanden()){
        weitermachen = false;
    }
    if(weitermachen){
    boolean Links = huegelVorhanden("links");
    boolean Rechts = huegelVorhanden("rechts");
    int Summe = 0;
    if(Links) {
        Summe +=1;
    }
    if(Rechts) {
        Summe +=2;
    }
    if(Summe==2){
        drehe("links");
    }
    if(Summe==1){
        drehe("rechts");
    }
    
    
        fahre();
        findOutsideLabyrint();
    }
    }
    
    public void fahreAcht(){
    drehe("links"); //1
    fahre();//2
    drehe("rechts");//3
    fahre();//4
    fahre();//5
    drehe("rechts");//6
    fahre();//7
    drehe("links");//8
    while(!huegelVorhanden("vorne"))//9
    fahre();//10
    drehe("rechts");//11
    fahre();//12
    drehe("links");
    fahre();//14
    fahre();
    drehe("links");//16
    fahre();
    fahre();
    drehe("links");
    fahre();//20
    fahre();//21
    drehe("links");//22
    fahre();//23
    drehe("rechts");
    while(!huegelVorhanden("vorne"))//25
    fahre();
    drehe("links");//27
    fahre();
    drehe("rechts");
    fahre();//30
    fahre();
    drehe("rechts");
    fahre();
    drehe("rechts");
    
    }

    public boolean OrientiereDichNachLinks()
    {
    int Findeposx1 = getX();
    int Findeposy1 = getY();
    
    fahre();
    
    int Findeposx2 = getX();
    int Findeposy2 = getY();
    int orientierung = 0;
    if(Findeposx1 < Findeposx2)
    {
        drehe("links");
        drehe("links");
}
    if(Findeposx1 > Findeposx2)
    System.out.println("passt");
    if(Findeposy1 < Findeposy2)
    drehe("rechts");
    if(Findeposy2 > Findeposy2)
    drehe("links");
    if(Findeposy1 == Findeposy2 | Findeposx1 == Findeposx2)
    return false;
    else return true;
    }
    
    
    public void FindeAusLabyrint()
    {
        int Rechts35 = 0;
        int Links35 = 0;
        int Vorne35= 0;
        int Fortsetzen35=1;
        //BasisWerte Analysieren
        if(huegelVorhanden("rechts"))
        Rechts35 = 1;
        
        if(huegelVorhanden("links"))
        Links35 = 2;
        
        if(huegelVorhanden("vorne"))
        Vorne35 = 4;
        
        
        //Summe Berrechnen
        int Summe35 = Rechts35 + Links35 + Vorne35;
        
        //Wenn Gestein Vorhanden ist Analysieren
        if(gesteinVorhanden())
        analysiereGestein();
        
        //Wenn Marke Vorhanden Rekursive Metode Stoppen
        if(markeVorhanden())
        {
            Fortsetzen35=0;
            nachricht("Sie Haben ihr Ziel er Erreicht ihr Ziel befindet sich links");
        }
        //Suf Basis Der Summe Handeln
        if(Fortsetzen35==1)
        {
        if(Summe35 == 1)
        fahre();
        else if(Summe35 == 3)
        fahre();
        else if(Summe35 == 5)
        drehe("links");
        else if(Summe35 == 7)
        {
        drehe("links");
        drehe("links");
        }
        else if(Summe35 == 0)
        {
        drehe("rechts");
        fahre();
        }
        else if(Summe35 == 2)
        {
            drehe("rechts");
            fahre();
        }
        else if(Summe35 == 4)
        Fortsetzen35=0;
        else if(Summe35 == 6)
        Fortsetzen35=0;
        }
        
        //Wenn  Fortsetzen30 1 ist Soll die Methode sich selber Aufrufen
        if(Fortsetzen35==1)
        FindeAusLabyrint();
    }
    
    public void findeallesGesteinundGeheAnUrsprungZurück()
    {
    int MerkePositionY = getY();
    int MerkePositionX = getX();
    fahre();
    fagungauz(MerkePositionX, MerkePositionY);
    }
    
    
    public void fagungauz(int XPosition, int YPosition)
    {
    int Weiterfahren = 1;
    if(XPosition == getX())
    if(YPosition == getY())
    Weiterfahren = 0;
    if(Weiterfahren == 1)
    {
    int Rechts39 = 0;
    int Links39 = 0;
    int Vorne39= 0;
    //BasisWerte Analysieren
        if(huegelVorhanden("rechts"))
        Rechts39 = 1;
        
        if(huegelVorhanden("links"))
        Links39 = 2;
        
        if(huegelVorhanden("vorne"))
        Vorne39 = 4;
        
        
        //Summe Berrechnen
        int Summe39 = Rechts39 + Links39 + Vorne39;
        
        //Wenn Gestein Vorhanden ist Analysieren
        if(gesteinVorhanden())
        analysiereGestein();
        
        
        //Suf Basis Der Summe Handeln
        if(Summe39 == 1)
        fahre();
        else if(Summe39 == 3)
        fahre();
        else if(Summe39 == 5)
        drehe("links");
        else if(Summe39 == 7)
        {
        drehe("links");
        drehe("links");
        }
        else if(Summe39 == 0)
        {
        drehe("rechts");
        fahre();
        }
        
    int XPosition2 = XPosition;
    int YPosition2 = YPosition;
        

        fagungauz(XPosition2,YPosition2);
    }
    
    }
    
    public void findeEinenUnbefahrbahrenBereich()
    {
    int posX70 = getX();
    int Zähler2561 = posX70;
    int Wertehalber = 0;
    int posY70 = getY();
    if (posX70==1)
    {
    Wertehalber =+1;
    }
    else
    {
    drehe("rechts");
    drehe("rechts");
    fahre();
    fahre();
    fahre();
    fahre();
    fahre();
    fahre();
    fahre();
    fahre();
    fahre();
    fahre();
    fahre();
    fahre();
    fahre();
    fahre();
    fahre();
    fahre();
    fahre();
    fahre();
    fahre();
    fahre();
    fahre();
    drehe("rechts");
    drehe("rechts");
    }
    if(posY70==0)
    Wertehalber=+1;
    else
    {
    drehe("links");
    fahre();
    fahre();
    fahre();
    fahre();
    fahre();
    fahre();
    fahre();
    fahre();
    fahre();
    fahre();
    fahre();
    fahre();
    fahre();
    fahre();
    fahre();
    fahre();
    fahre();
    fahre();
    fahre();
    fahre();
    fahre();
    drehe("rechts");
    }
    
    while(!huegelVorhanden("vorne"))
    fahreZeileAb();
    
    Koordinaten koordinaten1 = analysierGrößeHuegelteil();
    
    int X82 = koordinaten1.getX1();
    int X93 = koordinaten1.getX2();
    int Y92 = koordinaten1.getY1();
    int Y93 = koordinaten1.getY2();
    nachricht("X1 = "+ X82 + "Y1 "+Y92+"X2"+X93+"Y2"+Y93);
    }
    public void fahreZeileAb()
    {
    while(!huegelVorhanden("vorne"))
    {
    fahre();
    fahre();
    fahre();
    fahre();
    fahre();
    fahre();
    fahre();
    fahre();
    fahre();
    fahre();
    fahre();
    fahre();
    fahre();
    fahre();
    fahre();
    fahre();
    fahre();
    if(huegelVorhanden("vorne"))
    {
    drehe("rechts");
    drehe("rechts");
    fahre();
    fahre();
    fahre();
    fahre();
    fahre();
    fahre();
    fahre();
    fahre();
    fahre();
    fahre();
    fahre();
    fahre();
    fahre();
    fahre();
    fahre();
    fahre();
    drehe("links");
    fahre();
    drehe("links");
    }   
    }
    }
    
    public Koordinaten analysierGrößeHuegelteil()
    {
    setzeMarke();
    drehe("links");
    fahre();
    drehe("rechts");
    while(huegelVorhanden("rechts"))
    fahre();
    int X71 = getX();
    int Y71 = getY();
    drehe("rechts");
    while(huegelVorhanden("rechts"))
    fahre();
    int X72 = getX();
    int Y72 = getY();
    return new Koordinaten(X71, Y71, X72, Y72);
    }
    
    
    /**
     * Der Rover bewegt sich ein Feld in Fahrtrichtung weiter.
     * Sollte sich in Fahrtrichtung ein Objekt der Klasse Huegel befinden oder er sich an der Grenze der Welt befinden,
     * dann erscheint eine entsprechende Meldung auf dem Display.
     */
    
    public void fahreAnHuegelkette()
    {
        if(huegelVorhanden("rechts"))
        {
            while(huegelVorhanden("rechts"))
            fahre();
        }
    }
    
    public void fahreUmGebirge()
    {
        int Rechts30 = 0;
        int Links30 = 0;
        int Vorne30= 0;
        int Fortsetzen30=1;
        //BasisWerte Analysieren
        if(huegelVorhanden("rechts"))
        Rechts30 = 1;
        
        if(huegelVorhanden("links"))
        Links30 = 2;
        
        if(huegelVorhanden("vorne"))
        Vorne30 = 4;
        
        
        //Summe Berrechnen
        int Summe30 = Rechts30 + Links30 + Vorne30;
        
        //Wenn Gestein Vorhanden ist Analysieren
        if(gesteinVorhanden())
        analysiereGestein();
        
        //Wenn Marke Vorhanden Rekursive Metode Stoppen
        if(markeVorhanden())
        Fortsetzen30=0;
        
        //Suf Basis Der Summe Handeln
        if(Summe30 == 1)
        fahre();
        else if(Summe30 == 3)
        fahre();
        else if(Summe30 == 5)
        drehe("links");
        else if(Summe30 == 7)
        {
        drehe("links");
        drehe("links");
        }
        else if(Summe30 == 0)
        {
        drehe("rechts");
        fahre();
        }
        else if(Summe30 == 2)
        Fortsetzen30=0;
        else if(Summe30 == 4)
        Fortsetzen30=0;
        else if(Summe30 == 6)
        Fortsetzen30=0;
        
        
        //Wenn  Fortsetzen30 1 ist Soll die Methode sich selber Aufrufen
        if(Fortsetzen30==1)
        fahreUmGebirge();
    }
    public void fahre()
    {
        int posX = getX();
        int posY = getY();

        if(huegelVorhanden("vorne"))
        {
            nachricht("Zu steil!");
        }
        else if(getRotation()==270 && getY()==1)
        {
            nachricht("Ich kann mich nicht bewegen");
        }
        else
        {
            move(1);
            strecke++;
            Greenfoot.delay(1);
            //string temporär = name + " " +strecke;
            nachricht(name + " Gefahrene Strecke:" +strecke);
            nachricht("Anzahl Gefundener Steine: "+anzahlGefundenerSteine);
        }

        if(posX==getX()&&posY==getY()&&!huegelVorhanden("vorne"))
        {
            nachricht("Ich kann mich nicht bewegen");
        }
    }

    /**
     * Der Rover dreht sich um 90 Grad in die Richtung, die mit richtung („links“ oder „rechts“) übergeben wurde.
     * Sollte ein anderer Text (String) als "rechts" oder "links" übergeben werden, dann erscheint eine entsprechende Meldung auf dem Display.
     */
    public void drehe(String richtung)
    {
        if(richtung=="rechts")
        {
            setRotation(getRotation()+90);
        }
        else if (richtung=="links")
        {
            setRotation(getRotation()-90);
        }
        else
        {
            nachricht("Befehl nicht korrekt!");
        }
    }

    /**
     * Der Rover gibt durch einen Wahrheitswert (true oder false )zurück, ob sich auf seiner Position ein Objekt der Klasse Gestein befindet.
     * Eine entsprechende Meldung erscheint auch auf dem Display.
     */
    public boolean gesteinVorhanden()
    {
        if(getOneIntersectingObject(Gestein.class)!=null)
        {
            nachricht("Gestein gefunden!");
            return true;

        }

        return false;
    }

    /**
     * Der Rover überprüft, ob sich in richtung ("rechts", "links", oder "vorne") ein Objekt der Klasse Huegel befindet.
     * Das Ergebnis wird auf dem Display angezeigt.
     * Sollte ein anderer Text (String) als "rechts", "links" oder "vorne" übergeben werden, dann erscheint eine entsprechende Meldung auf dem Display.
     */
    public boolean huegelVorhanden(String richtung)
    {
        int rot = getRotation();

        if (richtung=="vorne" && rot==0 || richtung=="rechts" && rot==270 || richtung=="links" && rot==90)
        {
            if(getOneObjectAtOffset(1,0,Huegel.class)!=null && ((Huegel)getOneObjectAtOffset(1,0,Huegel.class)).getSteigung() >30)
            {
                return true;
            }
        }

        if (richtung=="vorne" && rot==180 || richtung=="rechts" && rot==90 || richtung=="links" && rot==270)
        {
            if(getOneObjectAtOffset(-1,0,Huegel.class)!=null && ((Huegel)getOneObjectAtOffset(-1,0,Huegel.class)).getSteigung() >30)
            {
                return true;
            }
        }

        if (richtung=="vorne" && rot==90 || richtung=="rechts" && rot==0 || richtung=="links" && rot==180)
        {
            if(getOneObjectAtOffset(0,1,Huegel.class)!=null && ((Huegel)getOneObjectAtOffset(0,1,Huegel.class)).getSteigung() >30)
            {
                return true;
            }

        }

        if (richtung=="vorne" && rot==270 || richtung=="rechts" && rot==180 || richtung=="links" && rot==0)
        {
            if(getOneObjectAtOffset(0,-1,Huegel.class)!=null && ((Huegel)getOneObjectAtOffset(0,-1,Huegel.class)).getSteigung() >30)
            {
                return true;
            }

        }

        if(richtung!="vorne" && richtung!="links" && richtung!="rechts")
        {
            nachricht("Befehl nicht korrekt!");
        }

        return false;
    }

    /**
     * Der Rover ermittelt den Wassergehalt des Gesteins auf seiner Position und gibt diesen auf dem Display aus.
     * Sollte kein Objekt der Klasse Gestein vorhanden sein, dann erscheint eine entsprechende Meldung auf dem Display.
     */
    public void analysiereGestein()
    {
        if(gesteinVorhanden())
        {
            nachricht("Gestein untersucht! Wassergehalt ist " + ((Gestein)getOneIntersectingObject(Gestein.class)).getWassergehalt()+"%.");
            Greenfoot.delay(1);
            removeTouching(Gestein.class);
        }
        else 
        {
            nachricht("Hier ist kein Gestein");
        }
    }

    /**
     * Der Rover erzeugt ein Objekt der Klasse „Markierung“ auf seiner Position.
     */
    public void setzeMarke()
    {
        getWorld().addObject(new Marke(), getX(), getY());
    }

    /**
     * *Der Rover gibt durch einen Wahrheitswert (true oder false )zurück, ob sich auf seiner Position ein Objekt der Marke befindet.
     * Eine entsprechende Meldung erscheint auch auf dem Display.
     */
    public boolean markeVorhanden()
    {
        if(getOneIntersectingObject(Marke.class)!=null)
        {
            return true;
        }

        return false;
    }

    public void entferneMarke()
    {
        if(markeVorhanden())
        {
            removeTouching(Marke.class);
        }
    }

    private void nachricht(String pText)
    {
        if(anzeige!=null)
        {
            anzeige.anzeigen(pText);
            Greenfoot.delay(1);
            anzeige.loeschen();
        }
    }

    private void displayAusschalten()
    {
        getWorld().removeObject(anzeige);

    }

    protected void addedToWorld(World world)
    {

        setImage("images/rover.png");
        world = getWorld();
        anzeige = new Display();
        anzeige.setImage("images/nachricht.png");
        world.addObject(anzeige, 7, 0);
        if(getY()==0)
        {
            setLocation(getX(),1);
        }
        anzeige.anzeigen("Ich bin bereit");

    }

    class Display extends Actor
    {
        GreenfootImage bild; 

        public Display()
        {
          bild = getImage();
        }

        public void act() 
        {

        }  

        public void anzeigen(String pText)
        {
           loeschen();
           getImage().drawImage(new GreenfootImage(pText, 25, Color.BLACK, new Color(0, 0, 0, 0)),10,10);

        }

        public void loeschen()
        {
            getImage().clear();
            setImage("images/nachricht.png");
        }

    }
}
